package com.jcaptcha1.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	private ImageCaptchaService captchaService;
	protected String captchaResponseParameterName ="j_captcha_response";

	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	/*@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	*/
	@RequestMapping(value="/")
	public String showPage(ModelMap map){
		map.addAttribute("comment",new Comment());
		return "form";
	}
	
	@RequestMapping(value="/submit")
	public String success(){
		return "success";
	}
	@RequestMapping(value="/captcha")
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
			byte[] captchaChallengeAsJpeg = null;
			// the output stream to render the captcha image as jpeg into
	        ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
	        
	    	// get the session id that will identify the generated captcha. 
	    	//the same id must be used to validate the response, the session id is a good candidate!
	    	String captchaId = request.getSession().getId();
	    	
	    	// call the ImageCaptchaService getChallenge method
	       	BufferedImage challenge =
	                captchaService.getImageChallengeForID(captchaId,request.getLocale());
	        
	        // a jpeg encoder
	        JPEGImageEncoder jpegEncoder =
	                JPEGCodec.createJPEGEncoder(jpegOutputStream);
	        jpegEncoder.encode(challenge);
	     

	        captchaChallengeAsJpeg = jpegOutputStream.toByteArray();

	        // flush it in the response
	        response.setHeader("Cache-Control", "no-store");
	        response.setHeader("Pragma", "no-cache");
	        response.setDateHeader("Expires", 0);
	        response.setContentType("image/jpeg");
	        ServletOutputStream responseOutputStream =
	        	response.getOutputStream();
	        responseOutputStream.write(captchaChallengeAsJpeg);
	        responseOutputStream.flush();
	        responseOutputStream.close();
	        return null;
		}

		/**
		 * Set captcha service
		 * @param captchaService The captchaService to set.
		 */
		public void setCaptchaService(ImageCaptchaService captchaService) {
			this.captchaService = captchaService;		
		}	
		
		/**
		 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
		 */
	/*	public void afterPropertiesSet() throws Exception {
			if(captchaService == null){
				throw new RuntimeException("Image captcha service wasn`t set!");
			}
			}
		*/

	@RequestMapping(value="/submitForm")
	public String submitForm(@ModelAttribute(value="comment")Comment comment,HttpServletRequest request,ModelMap map){
		boolean isResponseCorrect = false;
		 String captchaId = request.getSession().getId();
	        //retrieve the response
	     String response = request.getParameter(captchaResponseParameterName);
	        //validate response
	        try {			
				if(response != null){
					isResponseCorrect =
						captchaService.validateResponseForID(captchaId, response);
					if(isResponseCorrect==true){
						map.addAttribute("msg","WellDone");
					}
					else{
						map.addAttribute("msg","Captcha Input is Wrong Try Again");
					}
				}
			} catch (CaptchaServiceException e) {
			    //should not happen, may be thrown if the id is not valid			
			}
			
		/*	if(!isResponseCorrect){
				//prepare object error, captcha response isn`t valid
		        //String objectName = "Captcha";
				//String[] codes = {"captchaerror"};
				//Object[] arguments = {};
				//String defaultMessage = "Wrong cotrol text!";
				//ObjectError oe = new ObjectError(objectName, codes, arguments, defaultMessage);
				//errors.addError(oe);
				map.addAttribute("msg","input captcha value is wrong");
			return "form";
			}         
return "success";*/
	
	
	return "form";
	}

	
}
